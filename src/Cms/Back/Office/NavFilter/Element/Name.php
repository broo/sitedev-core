<?php

namespace Core\Cms\Back\Office\NavFilter\Element;

use App\Cms\Back\Office\NavFilter\Element;
use Ext\Db;

abstract class Name extends Element
{
    public function __construct($_title = null)
    {
        parent::__construct(null, $_title);
    }

    public function getSqlWhere()
    {
        $where = array();

        if ($this->_value !== false) {
            $where[] = 'CONCAT_WS(" ", last_name, first_name, middle_name) LIKE ' .
                       Db::escape('%' . $this->_value . '%');
        }

        return $where;
    }
}
