<?php

namespace Core\Cms\Back\Office\NavFilter\Element;

use Ext\Db;

class ManyToMany extends Multiple
{
    protected $_key;
    protected $_table;
    protected $_valueKey;
    protected $_linkTable;

    public function __construct($_title, $_key, $_table, $_valueKey, $_linkTable)
    {
        parent::__construct(null, $_title);

        $this->_key = $_key;
        $this->_table = $_table;
        $this->_valueKey = $_valueKey;
        $this->_linkTable = $_linkTable;
    }

    public function getSqlWhere()
    {
        $where = array();

        if ($this->_value !== false) {
            $p = Db::get()->getPrefix();

            if ($this->_value) {
                $value = Db::escape($this->_value);

                $ids = Db::get()->getList("
                    SELECT   $this->_key
                    FROM     $this->_linkTable
                    WHERE    $this->_valueKey IN ($value)
                    GROUP BY $this->_key
                ");

            } else {
                $ids = Db::get()->getList("
                    SELECT    $this->_key
                    FROM      $this->_table
                    LEFT JOIN $this->_linkTable
                    USING     ($this->_key)
                    WHERE     ISNULL($this->_valueKey)
                ");
            }

            if (count($ids) == 0) {
                return false;
            }

            $value = Db::escape($ids);
            $where[] = "$this->_key IN ($value)";
        }

        return $where;
    }
}
